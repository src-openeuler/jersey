%bcond_with jp_minimal
Name:                jersey
Version:             2.29.1
Release:             3
Summary:             JAX-RS (JSR 311) production quality Reference Implementation
License:             (EPL-2.0 or GPLv2 with exceptions) and ASL 2.0
URL:                 https://github.com/eclipse-ee4j/jersey
Source0:             https://github.com/eclipse-ee4j/jersey/archive/%{version}/%{name}-%{version}.tar.gz
Source1:             http://www.apache.org/licenses/LICENSE-2.0.txt
Patch0:              jersey-2.17-mvc-jsp-servlet31.patch
Patch1:              0001-Patch-out-dependency-on-JMockit.patch
Patch2:              0002-Port-to-glassfish-jsonp-1.0.patch
Patch3:              0001-Port-to-hibernate-validation-5.x.patch
Patch4:              CVE-2021-28168.patch
BuildRequires:       maven-local mvn(com.fasterxml.jackson.core:jackson-annotations)
BuildRequires:       mvn(com.fasterxml.jackson.core:jackson-databind)
BuildRequires:       mvn(com.fasterxml.jackson.module:jackson-module-jaxb-annotations)
BuildRequires:       mvn(com.google.guava:guava:18.0)
BuildRequires:       mvn(com.sun.istack:istack-commons-maven-plugin) mvn(com.sun:tools)
BuildRequires:       mvn(jakarta.ws.rs:jakarta.ws.rs-api) mvn(javax.annotation:javax.annotation-api)
BuildRequires:       mvn(javax.inject:javax.inject) mvn(javax.xml.bind:jaxb-api)
BuildRequires:       mvn(javax.validation:validation-api) mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.apache.httpcomponents:httpclient)
BuildRequires:       mvn(org.apache.maven.plugins:maven-enforcer-plugin)
BuildRequires:       mvn(org.codehaus.mojo:build-helper-maven-plugin)
BuildRequires:       mvn(org.eclipse.jetty:jetty-client) mvn(org.glassfish.hk2:hk2-bom:pom:)
BuildRequires:       mvn(org.glassfish.hk2:hk2-locator) mvn(org.glassfish.hk2:osgi-resource-locator)
BuildRequires:       mvn(org.osgi:org.osgi.core) mvn(org.ow2.asm:asm) mvn(xerces:xercesImpl)
%if %{without jp_minimal}
BuildRequires:       mvn(com.github.spullara.mustache.java:compiler) mvn(io.reactivex:rxjava)
BuildRequires:       mvn(javax.el:javax.el-api) mvn(javax.enterprise:cdi-api)
BuildRequires:       mvn(javax.json:javax.json-api) mvn(javax.persistence:persistence-api)
BuildRequires:       mvn(javax.servlet:javax.servlet-api) mvn(javax.servlet.jsp:jsp-api)
BuildRequires:       mvn(javax.servlet:servlet-api) mvn(junit:junit)
BuildRequires:       mvn(org.codehaus.jettison:jettison) mvn(org.eclipse.jetty:jetty-continuation)
BuildRequires:       mvn(org.eclipse.jetty:jetty-server) mvn(org.eclipse.jetty:jetty-util)
BuildRequires:       mvn(org.eclipse.jetty:jetty-webapp) mvn(org.freemarker:freemarker)
BuildRequires:       mvn(org.glassfish.grizzly:grizzly-http-server)
BuildRequires:       mvn(org.glassfish.grizzly:grizzly-http-servlet) mvn(org.glassfish:javax.el)
BuildRequires:       mvn(org.glassfish:javax.json) mvn(org.glassfish:jsonp-jaxrs)
BuildRequires:       mvn(org.hamcrest:hamcrest-library) mvn(org.hibernate:hibernate-validator)
BuildRequires:       mvn(org.hibernate:hibernate-validator-cdi) mvn(org.jboss:jboss-vfs)
BuildRequires:       mvn(org.jboss.spec.javax.interceptor:jboss-interceptors-api_1.2_spec)
BuildRequires:       mvn(org.jboss.spec.javax.transaction:jboss-transaction-api_1.2_spec)
BuildRequires:       mvn(org.jboss.weld.se:weld-se-core) mvn(org.jvnet.mimepull:mimepull)
BuildRequires:       mvn(org.mockito:mockito-all) mvn(org.simpleframework:simple-common)
BuildRequires:       mvn(org.simpleframework:simple-http) mvn(org.simpleframework:simple-transport)
BuildRequires:       mvn(org.testng:testng) mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:       mvn(org.glassfish.hk2:hk2) mvn(org.glassfish.hk2:spring-bridge) mvn(org.aspectj:aspectjrt) mvn(org.aspectj:aspectjweaver) mvn(org.springframework:spring-aop)
BuildRequires:       mvn(org.springframework:spring-beans) mvn(org.springframework:spring-beans) mvn(org.springframework:spring-core)  mvn(org.springframework:spring-web) mvn(org.springframework:spring-aop) 
#BuildRequires:       mvn(org.eclipse.microprofile.config:microprofile-config-api) mvn(org.eclipse.microprofile.rest.client:microprofile-rest-client-api) mvn(javax.json:javax.json-api)
#BuildRequires:       mvn(org.eclipse.microprofile.config:microprofile-config-api) mvn(io.helidon.microprofile.config:helidon-microprofile-config)
%endif
BuildArch:           noarch
%description
Jersey is the open source JAX-RS (JSR 311)
production quality Reference Implementation
for building RESTful Web services.
%if %{without jp_minimal}

%package test-framework
Summary:             Jersey Test Framework
%description test-framework
%{summary}.
%endif

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains javadoc for %{name}.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
find . -name "*.jar" -print -delete
find . -name "*.class" -print -delete
cp -p %{SOURCE1} .
sed -i 's/\r//' LICENSE-2.0.txt
#rm -r core-server/src/main/java/jersey
#find core-server -name "*.java" -exec sed -i "s|jersey.repackaged.||" {} +
#rm -r core-common/src/main/java/org/glassfish/jersey/internal/guava
#grep -rl --include=*.java org.glassfish.jersey.internal.guava | xargs sed -i "s|org\.glassfish\.jersey\.internal\.guava|com.google.common.base|"
find core-* containers/{grizzly2,jdk,jetty}-http media/sse ext/{entity-filtering,bean-validation,rx} -name "*.java" -exec sed -i \
  -e "/base\.Cache/s/common\.base/common.cache/" \
  -e "/base\.LoadingCache/s/common\.base/common.cache/" \
  -e "/base\.Multimap/s/common\.base/common.collect/" \
  -e "/base\.....Multimap/s/common\.base/common.collect/" \
  -e "/base\.HashBasedTable/s/common\.base/common.collect/" \
  -e "/base\.Table/s/common\.base/common.collect/" \
  -e "/base\.ThreadFactoryBuilder/s/common\.base/common.util.concurrent/" \
  -e "/base\.InetAddresses/s/common\.base/common.net/" \
  -e "/base\.Primitives/s/common\.base/common.primitives/" {} +
%pom_add_dep 'com.google.guava:guava:${guava.version}' core-common inject/hk2
%pom_xpath_set "pom:dependency[pom:artifactId = 'guava']/pom:scope" provided containers/jdk-http
%pom_add_dep 'org.ow2.asm:asm:${asm.version}' core-server
%pom_remove_parent bom .
%pom_change_dep -r jakarta.servlet:jakarta.servlet-api javax.servlet:javax.servlet-api . test-framework
%pom_change_dep -r jakarta.servlet.jsp:jakarta.servlet.jsp-api javax.servlet.jsp:jsp-api
%pom_change_dep -r jakarta.xml.bind:jakarta.xml.bind-api javax.xml.bind:jaxb-api
%pom_change_dep -r jakarta.annotation:jakarta.annotation-api javax.annotation:javax.annotation-api
%pom_change_dep -r jakarta.persistence:jakarta.persistence-api javax.persistence:persistence-api
%pom_change_dep -r org.glassfish.hk2.external:jakarta.inject javax.inject:javax.inject
%pom_change_dep -r jakarta.el:jakarta.el-api javax.el:javax.el-api
%pom_change_dep -r org.glassfish:jakarta.el org.glassfish:javax.el
%pom_change_dep -r org.glassfish:jakarta.json org.glassfish:javax.json
%pom_change_dep -r jakarta.validation:jakarta.validation-api javax.validation:validation-api
%pom_change_dep -r jakarta.json:jakarta.json-api javax.json:javax.json-api 
%pom_add_dep javax.json:javax.json-api:1.0 media/json-processing
%pom_change_dep javax:javaee-api javax.enterprise:cdi-api:'${cdi.api.version}':provided ext/cdi/jersey-cdi1x-transaction
%pom_add_dep org.jboss.spec.javax.transaction:jboss-transaction-api_1.2_spec:1.0.0.Alpha3:provided ext/cdi/jersey-cdi1x-transaction
%pom_add_dep org.jboss.spec.javax.interceptor:jboss-interceptors-api_1.2_spec:1.0.0.Alpha3:provided ext/cdi/jersey-cdi1x-validation
%pom_xpath_remove "pom:dependencies/pom:dependency[pom:artifactId = 'tools']/pom:scope" ext/wadl-doclet
%pom_xpath_remove "pom:dependencies/pom:dependency[pom:artifactId = 'tools']/pom:systemPath" ext/wadl-doclet
%pom_remove_dep -r org.mortbay.jetty:servlet-api-2.5
%pom_remove_dep -r org.jmockit:jmockit
%pom_xpath_remove pom:build/pom:extensions
%pom_remove_plugin :buildnumber-maven-plugin
%pom_remove_plugin :buildnumber-maven-plugin core-common
%pom_remove_plugin :findbugs-maven-plugin
%pom_remove_plugin :maven-checkstyle-plugin
%pom_remove_plugin :maven-source-plugin
%pom_remove_plugin :maven-jflex-plugin media/moxy
%pom_remove_plugin :maven-jflex-plugin media/jaxb
%pom_xpath_remove "pom:plugin[pom:artifactId = 'maven-javadoc-plugin' ]/pom:executions"
%pom_disable_module archetypes
%pom_disable_module incubator
%pom_disable_module netty-http containers
%pom_remove_dep :jersey-container-netty-http bom
%pom_disable_module netty-connector connectors
%pom_remove_dep :jersey-netty-connector bom
%pom_disable_module netty test-framework/providers
%pom_remove_dep :jersey-test-framework-provider-netty test-framework/providers/bundle
%pom_disable_module servlet-portability ext
%pom_remove_dep :jersey-servlet-portability bom
%pom_disable_module json-jackson1 media
%pom_remove_dep :jersey-media-json-jackson1 bom
%pom_disable_module json-binding media
%pom_remove_dep :jersey-media-json-binding bom
%pom_disable_module moxy media
%pom_remove_dep :jersey-media-moxy bom
%pom_disable_module spring4 ext
%pom_remove_dep :jersey-spring4 bom
%pom_disable_module grizzly-connector connectors
%pom_remove_dep :jersey-grizzly-connector bom
%pom_remove_dep org.glassfish.jersey.connectors:jersey-grizzly-connector media/multipart
%pom_disable_module mp-rest-client ext/microprofile
%pom_disable_module mp-config ext/microprofile
rm media/multipart/src/test/java/org/glassfish/jersey/media/multipart/internal/MultiPartHeaderModificationTest.java
%pom_disable_module glassfish containers
%pom_remove_dep :jersey-gf-ejb bom
%pom_disable_module cdi2-se inject
%pom_remove_dep :jersey-cdi2-se bom
%pom_disable_module rx-client-rxjava2 ext/rx
%pom_remove_dep :jersey-rx-client-rxjava2 bom
%pom_disable_module maven test-framework
%pom_remove_plugin com.sun.tools.xjc.maven2: core-server
%if %{with jp_minimal}
%pom_disable_module bom
%pom_disable_module containers
%pom_disable_module security
%pom_disable_module json-jettison media
%pom_disable_module json-processing media
%pom_disable_module multipart media
%pom_disable_module sse media
%pom_disable_module bean-validation ext
%pom_disable_module cdi ext
%pom_disable_module metainf-services ext
%pom_disable_module mvc ext
%pom_disable_module mvc-bean-validation ext
%pom_disable_module mvc-freemarker ext
%pom_disable_module mvc-jsp ext
%pom_disable_module mvc-mustache ext
%pom_disable_module proxy-client ext
%pom_disable_module rx ext
%pom_disable_module microprofile ext
%endif
%pom_xpath_inject "pom:plugin/pom:configuration/pom:instructions" \
  '<Require-Bundle>org.glassfish.jersey.inject.jersey-hk2;bundle-version="%{version}"</Require-Bundle>' core-common
cp -p inject/hk2/src/main/resources/META-INF/services/org.glassfish.jersey.internal.inject.InjectionManagerFactory \
  core-common/src/main/resources/META-INF/services
sed -i -e 's/javax\.annotation\.\*;version="!"/javax.annotation.*/' $(find -name pom.xml)
sed -i -e 's/javax\.activation\.\*;/javax.activation.*;resolution:=optional;/' core-common/pom.xml
%mvn_file "org.glassfish.jersey.connectors:project" %{name}/connectors-project
%mvn_file "org.glassfish.jersey.containers:project" %{name}/containers-project
%mvn_file "org.glassfish.jersey.ext:project" %{name}/ext-project
%mvn_file "org.glassfish.jersey.ext.cdi:project" %{name}/ext-cdi-project
%mvn_file "org.glassfish.jersey.ext.rx:project" %{name}/ext-rx-project
%mvn_file "org.glassfish.jersey.inject:project" %{name}/inject-project
%mvn_file "org.glassfish.jersey.media:project" %{name}/media-project
%mvn_file "org.glassfish.jersey.security:project" %{name}/security-project
%mvn_file "org.glassfish.jersey.test-framework:project" %{name}/test-framework-project
%mvn_file "org.glassfish.jersey.test-framework.providers:project" %{name}/test-framework-providers-project
%mvn_file "org.glassfish.jersey.ext.microprofile:project" %{name}/ext-microprofile-project
%mvn_package "org.glassfish.jersey.test-framework*:" test-framework

%build
%if %{without jp_minimal}
%mvn_build -- -PsecurityOff -Dasm.version=6.2.1 -Dmaven.test.failure.ignore=true \
  -Dexamples.excluded -Dtests.excluded -Dbundles.excluded -Dmaven.test.skip=true
%else
%mvn_build -f -- -PsecurityOff -Dasm.version=6.2.1 -Dmaven.test.failure.ignore=true \
  -Dexamples.excluded -Dtests.excluded -Dbundles.excluded -Dtest-framework.excluded \
  -Dmaven.test.skip=true
%endif

%install
%mvn_install

%files -f .mfiles
%doc README.md CONTRIBUTING.md
%license LICENSE.md NOTICE.md LICENSE-2.0.txt
%if %{without jp_minimal}

%files test-framework -f .mfiles-test-framework
%license LICENSE.md NOTICE.md LICENSE-2.0.txt
%endif

%files javadoc -f .mfiles-javadoc
%license LICENSE.md NOTICE.md LICENSE-2.0.txt

%changelog
* Wed Jan 03 2024 wangkai <13474090681@163.com> - 2.29.1-3
- fix CVE-2021-28168

* Fri 17 Sep 2021 wuzhen <wuzhen36@huawei.com> - 2.29.1-2
- Add "-Dmaven.skip.test=true" to skip maven test.

* Tue May 18 2021 guoxiaoqi2 <guoxiaoqi2@huawei.com> - 2.29.1-1
- update to 2.29.1

* Tue Aug 25 2020 Shaoqiang Kang <kangshaoqiang1@huawei.com> - 2.28-1
- Package init
